# VS Technology

Device control for VS Technology VST lighting power controller implemented in Python
- power supply (e.g. VLP-2430-2e) controlled via ethernet
 

1.  connect the device to DHCP server
2.  obtain IP address from DHCP server
    (e.g. 192.168.0.100)
3.  use class VLP ([VLP.py](VLP.py)) to communicate


Machine Vision Lighting Control Software for VS Technology MV lighting devices: VAL LIGHTING.
Can be used to control bar light, dome light, spot light, ring light, 
back light, etc. LED illumination, co-axial illumination via the VLP (Ethernet)
Intelligent digital power supply.

[VAL Lighting](https://vst.co.jp/en/category/lighting-en/) Device Control. VAL is an original brand under VS Technology Group.