# -*- coding: utf-8 -*-
"""
class VLP
    Interface to control the VLP-2430-2e and other
    power supplies by VS Technology via ethernet
    Note: VLP-2430-2e need to be connected to DHCP server 
          VLP-abcd-2eN can be connected without DHCP server
    
See demo() function below, for how to use it.

Created on Fri Sep 13 14:05:38 2019

@author: Udo Birk
(c)2019 FHGR University of Applied Sciences of the Grisons
"""

import socket 
import time
import sys

TCP_IP = '192.168.0.100'
DEBUG = False

class VLP:
    TCP_PORT = 50001
    BUFFER_SIZE = 1024
    CRLF = "\r\n"
    s = None
    channel = '00' # 00=CH1, 01=CH2, 02=CH3, etc.
    isError = False
    error_message = ''
    
    def __init__(self, tcpIP=TCP_IP):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(3)
        self.TCP_IP = tcpIP
        print("Connecting to ",tcpIP,":",self.TCP_PORT)
        try:
            self.s.connect((self.TCP_IP, self.TCP_PORT))
        except:
            self.isError = True
            self.error_message='connection failed'

    def send(self, msg = '@00L1'):
        if(self.isError):
            return(False)
        ba = bytearray(msg.upper().encode())
        chkSum = sum(ba) & 0xFF
        ba+=(hex(chkSum)[2:].upper()+self.CRLF).encode()
        if(DEBUG):
            print(ba)
        self.s.send(ba)
        data = self.s.recv(self.BUFFER_SIZE)
        if(DEBUG):
            print(data)
        else:
            time.sleep(0.1)

    def close(self):
        try:
            self.s.settimeout(None)
        except:
            self.error_message = sys.exc_info()[1]
            self.isError = True
        self.s.close()
        
    def set_intensity(self, val):
        if(self.isError):
            return(False)
        if((val < 0) or (val > 255)):
            self.isError = True
            self.error_message = 'intensity value exceeds range'
            return(False)
        print("Set intensity to: ",val)
        if(val > 0):
            msg='@{}F{:03d}'.format(self.channel, val)
            try:
                self.send(msg)
                self.turn_on()
            except:
                self.error_message = sys.exc_info()[1]
                self.isError = True
               
        else:
            self.off()

    def set_channel(self, val):
        self.channel = '{:02d}'.format(val)
        print("Set channel to:",self.channel)
        
    def turn_on(self):
        if(self.isError):
            return(False)
        self.send('@{}L1'.format(self.channel))

    def turn_off(self):
        if(self.isError):
            return(False)
        print("Set light channel {}: OFF".format(self.channel))
        self.send('@{}L0'.format(self.channel))
        
    def clear_error(self):
        self.isError = False
        self.error_message = ''


def demo():
    from VLP import VLP
    # open connection 
    s = VLP('192.168.0.100')
    if(s.isError):
        print(s.error_message)
        return
    
    # select channel 0 (=CH1)
    s.set_channel(0)
    # set intensity to 25
    s.set_intensity(25)
    time.sleep(1)
    # set intensity to 150
    s.set_intensity(150)
    time.sleep(1)
    # switch OFF
    s.turn_off()
    time.sleep(1)
    # set intensity to 50
    s.set_intensity(50)
    # select channel 0 (=CH1)
    s.set_channel(1)
    # switch OFF
    s.turn_off()
    time.sleep(1)
    # set intensity to 50
    s.set_intensity(50)
    s.close()
    

if __name__ == '__main__':
    demo()
    